var colorAxis = {
    'activecases': ['#ffecc9', '#ff6000'],
    'confirmed': ['#d8d6ff', '#0d05ff'],
    'deaths': ['#ffc4c4', '#ff2727'],
    'recovered': [ '#c3ffc3', '#0f0' ],
    'fatality': [ '#ffc4ff', '#f0f' ],
    'markers': [ '#f00', '#f00' ]
};

var tooltips = {};

var mapValueTitle = {
    "capitarecovered": "Recoveries per 1 million people",
    "capitadeaths": "Deaths per 1 million people",
    "capitaconfirmed": "Cases per 1 million people",
    "capitaactivecases": "Active Cases per 1 million people",
    "allfatality": "Fatality Rate",
    "allrecovered": "Recovered",
    "alldeaths": "Deaths",
    "allconfirmed": "Confirmed Cases",
    "allactivecases": "Active Cases",
    "24recovered": "New Recoveries",
    "24deaths": "New Deaths",
    "24confirmed": "New Cases",
};

var mapValueColumn = {
    "capitarecovered": "percRecovered",
    "capitadeaths": "percDeaths",
    "capitaconfirmed": "percCases",
    "capitaactivecases": "percActivecases",
    "allfatality": "fatality",
    "allrecovered": "recovered",
    "alldeaths": "deaths",
    "allconfirmed": "confirmed",
    "allactivecases": "activecases",
    "24recovered": "newrecoveries",
    "24deaths": "newdeaths",
    "24confirmed": "newcases",
};

var chart;

function createMap ( dataNice, region, mode ) {

    google.charts.setOnLoadCallback(function(){

      var dataGoogle = google.visualization.arrayToDataTable( dataNice );

      var options = {
          colorAxis: {colors: colorAxis[ mode ] },
          backgroundColor: '#181818',
          datalessRegionColor: '#888',
          magnifyingGlass: {enable: true, zoomFactor: 5.0},
          region: region,
          legend: 'none',
          height: vh,
          keepAspectRatio: true,
          tooltip: {isHtml: true}
      };

      chart = new google.visualization.GeoChart(document.getElementById('map'));

      chart.draw(dataGoogle, options);

    });

}

function createCountryTooltip( country ) {

    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {

        return '';

    }

    if ( tooltips.hasOwnProperty( country ) === true ) {

        return tooltips[ country ];

    }

    var html = '<div style="color:white;">' +
        '<h2>' + country + '</h2>' +
        '<h3>Total Data</h3>' +
        '<p><b>Fatality Rate: </b>' + data[ country ].fatality + '%</p>' +
        '<p><b>Active Cases: </b>' + data[ country ].activecases + '</p>' +
        '<p><b>Confirmed Cases: </b>' + data[ country ].confirmed + '</p>' +
        '<p><b>Deaths: </b>' + data[ country ].deaths + '</p>' +
        '<p><b>Recoveries: </b>' + data[ country ].recovered + '</p>' +
        '<h3>Last 24 hours</h3>' +
        '<p><b>New Cases: </b>' + data[ country ].newcases + '</p>' +
        '<p><b>New Deaths: </b>' + data[ country ].newdeaths + '</p>' +
        '<p><b>New Recoveries: </b>' + data[ country ].newrecoveries + '</p>' +
        '<h3>Per 1 million people</h3>' +
        '<p><b>Active Cases: </b>' + data[ country ].percActivecases + '</p>' +
        '<p><b>Confirmed Cases: </b>' + data[ country ].percCases + '</p>' +
        '<p><b>Deaths: </b>' + data[ country ].percDeaths + '</p>' +
        '<p><b>Recoveries: </b>' + data[ country ].percRecovered + '</p>' +
        '</div>';

    tooltips[ country ] = html;

    return tooltips[ country ];

}

function formCountryData( country, value ) {

    if ( value < 0 ) { value = 0; }

    var countryData = [];

    countryData.push( {v:data[ country ].g,f:'',c:country} );

    countryData.push( value );

    if ( ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {

        countryData.push( '' );

    } else {

        countryData.push( createCountryTooltip( country ) );

    }

    return countryData;

}

function formData( supermode, mode ) {

    if ( supermode === 'capita' && mode === 'fatality' ) {

        supermode = "all";

    } else if ( supermode === '24' && mode === 'fatality' ) {

        supermode = 'all';

    } else if ( supermode === '24' && mode === 'activecases' ) {

        supermode = 'all';

    }

    var dataRaw = [];

    dataRaw[0] = [ 'Country', mapValueTitle[ supermode + mode ], {'type': 'string', 'role': 'tooltip', 'p': {'html': true}} ];

    for ( var country in data ) {

        if ( country === "Cruise Ship (Princess Diamond)" ) { continue; }
        if ( country === "Channel Islands" ) { continue; }
        if ( country === "Saint Martin (French part)" ) { continue; }
        if ( country === "Tanzania" ) { continue; }
        if ( country === "Tanzania, United Republic of" ) { continue; }
        if ( country === "San Marino" ) { continue; }
        if ( country === "Faroe Islands" ) { continue; }
        if ( country === "Holy See" ) { continue; }

        var countryData = formCountryData( country, data[ country ][ mapValueColumn[ supermode + mode ] ] );

        dataRaw.push( countryData );

    }

    return dataRaw;

}

function drawMap() {

    var region      = 'world';
    var mode        = 'activecases';
    var superMode   = 'all';

    var dataNice = formData( superMode, mode );

    createMap( dataNice, region, mode );

}



var vw = Math.max( document.documentElement.clientWidth, window.innerWidth || 0) ;

var vh = Math.floor( vw * 0.621 );

document.getElementById( 'mapPanel' ).style.height = vh + 'px';

var script_gstatic = document.createElement('script');

script_gstatic.onload = function () {

    google.charts.load('current', {
        'packages':['geochart']
    });

    google.charts.setOnLoadCallback( drawMap );

};

script_gstatic.src = "https://www.gstatic.com/charts/loader.js";

document.head.appendChild( script_gstatic );
