'use strict';

self.addEventListener('push', function(event) {

  let notificationTitle = 'Hello';
  const notificationOptions = {
    body: 'Covid 19 Tracker updates',
    icon: './imgs/logo-512x512.png',
    badge: './imgs/logo-256x256.png',
    data: {
      url: 'https://covid19tracker.cc/',
    },
  };

  if (event.data) {

        var payload = event.data.json();

        notificationTitle = payload.title;
        notificationOptions.body = payload.message;
  }

  event.waitUntil(
    Promise.all([
      self.registration.showNotification(
        notificationTitle, notificationOptions),
      // self.analytics.trackEvent('push-received'),
    ])
  );
});

self.addEventListener('notificationclick', function(event) {

    let url = 'https://covid19tracker.cc/';
    event.notification.close();
    event.waitUntil(

        clients.matchAll({type: 'window'}).then( windowClients => {

            for ( var i = 0; i < windowClients.length; i++ ) {

                var client = windowClients[i];

                if ( client.url === url && 'focus' in client ) {

                    return client.focus();
                }

            }

            if ( clients.openWindow ) {

                return clients.openWindow( url );

            }
        })
    );
});

self.addEventListener('notificationclose', function(event) {
  event.waitUntil(
    Promise.all([
      // self.analytics.trackEvent('notification-close'),
    ])
  );
});
